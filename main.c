#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <ctype.h>
#define bits 16


void Cabecalho();
void ExibeMenu();
void LimpaTela();
void LinhaPequena();
void LinhaMedia();
void LinhaGrande();

int binToDec();
int decToBin();
int octToDec();
int decToOct();
int decToHex();
int hexToDec();

int potencia();

typedef char numero[bits];

numero binario = "01";
numero octal = "01234567";
numero decimal = "0123456789";
numero hexal = "0123456789ABCDEF";


int main () {

    numero entrada;
    setlocale(LC_ALL, "Portuguese");
    char opcao;
    int valido, inputDecimal;

    while (opcao != 'G' && opcao != 'g') {

        system("clear");
        Cabecalho();
        ExibeMenu();

        scanf("%c%*c", &opcao);
        fflush(stdin);

        switch(opcao) {

            case 'a':
            case 'A':
                valido = 0;
                while(valido != 1) {
                    system("cls");
                    LinhaGrande();
                    printf("         BINARIO PRA DECIMAL ");
                    LinhaGrande();
                    printf("\n Digite o n�mero Bin�rio a ser convertido: ");
                    gets(entrada);
                    binToDec(entrada, &valido);
                    system("\n pause");
                }
                break;

            case 'b':
            case 'B':
                LinhaPequena();
                valido = 0;
                while(valido != 1) {
                    system("cls");
                    LinhaGrande();
                    printf("\n         DECIMAL PRA BINARIO ");
                    LinhaGrande();
                    printf("\n Digite o n�mero Decimal a ser convertido: ");
                    scanf("%d", &inputDecimal);
                    fflush(stdin);
                    decToBin(inputDecimal, &valido);
                    printf("\n\n");
                    system("pause");
                }
                break;

            case 'c':
            case 'C':
                LinhaPequena();
                valido = 0;
                while(valido != 1) {
                    system("cls");
                    LinhaGrande();
                    printf("         OCTAL PRA DECIMAL ");
                    LinhaGrande();
                    printf("\nDigite o n�mero octal a ser convertido: ");
                    gets(entrada);
                    octToDec(entrada, &valido);
                    printf("\n\n");
                    system("pause");
                }

                break;

            case 'd':
            case 'D':
                LinhaPequena();
                valido = 0;
                while(valido != 1) {
                    system("cls");
                    LinhaGrande();
                    printf("\n         DECIMAL PRA OCTAL ");
                    LinhaGrande();
                    printf("\n Digite o n�mero Decimal a ser convertido: ");
                    scanf("%d", &inputDecimal);
                    fflush(stdin);
                    decToOct(inputDecimal, &valido);
                    printf("\n\n");
                    system("pause");
                }
                break;


            case 'e':
            case 'E':
                LinhaPequena();
                valido = 0;
                while(valido != 1) {
                    system("cls");
                    LinhaGrande();
                    printf("\n         DECIMAL PRA HEXADECIMAL ");
                    LinhaGrande();
                    printf("\n Digite o n�mero Decimal a ser convertido: ");
                    scanf("%d", &inputDecimal);
                    fflush(stdin);
                    decToHex(inputDecimal, &valido);
                    printf("\n\n");
                    system("pause");
                }
                break;


            case 'f':
            case 'F':
                LinhaPequena();
                valido = 0;
                while(valido != 1) {
                    system("cls");
                    LinhaGrande();
                    printf("         HEXADECIMAL PRA DECIMAL ");
                    LinhaGrande();
                    printf("\nDigite o n�mero Hexadecimal a ser convertido: ");
                    gets(entrada);
                    hexToDec(entrada, &valido);
                    printf("\n\n");
                    system("pause");
                }

                break;

            case 'g':
            case 'G':
                printf("\n\n");
                printf("\n Saindo... ");

                break;

            default:
                printf("\n\n");
                printf("\n Op��o inv�lida. Tente novamente! \n\n");

                break;
        }
    }
    LimpaTela();

    return 0;
}

int hexToDec(char entrada[bits], int *valido) {
    int i, j, decimal = 0, verifica = 0;;
    int pot;
    int num;
    numero analisado;
    strcpy(analisado, entrada);
    for (i = 0; i < strlen(analisado); i++) {
        for (j = 0; j < bits; j++) {
            if (toupper(analisado[i]) == hexal[j]){
                verifica++;
            }
        }
    }

    if (verifica != strlen(analisado)) {
        printf("\n Hexal inv�lido! \n\n");
        return 0;
    }
    else {
        *valido = 1;

        for (i = 0; i < strlen(analisado) ; i++) {
                int j = (strlen(analisado) - (i + 1));

                if (toupper((analisado[i])) == 'A')
                    num = 10;
                else if (toupper((analisado[i])) == 'B')
                    num = 11;
                else if (toupper((analisado[i])) == 'C')
                    num = 12;
                else if (toupper((analisado[i])) == 'D')
                    num = 13;
                else if ((toupper(analisado[i])) == 'E')
                    num = 14;
                else if (toupper((analisado[i])) == 'F')
                    num = 15;
                else num = analisado[i] - '0';

                pot = num * (potencia(16, j));
                decimal = decimal + pot;

        }
        LinhaGrande();
        printf("\n HEXADECIMAL: %s \n DECIMAL: %d", analisado, decimal);
        LinhaGrande();
    }

}

int decToHex(int entrada[bits], int *valido) {
    int i = 0, j, resto, input, count = 0;
    char backNum[bits];
    input = entrada;
    int num;
    if (input < 1) {
        printf("\n N�mero Decimal Inv�lido. \n");
        return 0;
    }

    while ( resto > 15 ) {
        num = input % 16;
        backNum[i] = hexal[num];
        resto = input/16;
        input = resto;
        i++;

    }

    backNum[i] = hexal[input];
    char numFinal[i];
    count = 0;
    LinhaGrande();
    printf("\n DECIMAL: %d \n HEXADECIMAL: ", entrada);
    for (j = i; j > -1; j--) {
        numFinal[count] = backNum[j];
        printf("%c", numFinal[count]);
        count++;
    }
    LinhaGrande();
    *valido =1;
}

int octToDec(char entrada[bits], int *valido) {
    int num, i, decimal = 0;
    int pot;
    numero analisado;
    strcpy(analisado, entrada);
    for (i = 0; i < strlen(analisado); i++) {
            if (analisado[i] == '8' || analisado[i] == '9'){
                printf("\n Octal Inv�lido \n\n");
                return 0;
            }
    }

    *valido = 1;

    for (i = 0; i < strlen(analisado); i++) {
        num = analisado[i] - '0';
        pot = num * (potencia(8, strlen(analisado) - (i + 1)));
        decimal = decimal + pot;
    }

    LinhaGrande();
    printf("\n OCTAL: %s \n DECIMAL: %d", analisado, decimal);
    LinhaGrande();

    return 0;
}

int binToDec(char entrada[bits], int *valido) {
    int i, decimal = 0;
    int pot;
    numero analisado;
    strcpy(analisado, entrada);
    for (i = 0; i < strlen(analisado); i++) {
            if (analisado[i] != '0' && analisado[i] != '1'){
                printf("\n Bin�rio inv�lido \n\n");
                return 0;
            }
    }

    *valido = 1;

    for (i = 0; i < strlen(analisado) ; i++) {
        if (analisado[i] == '1') {
            pot = potencia(2, strlen(analisado) - (i + 1));
            decimal = decimal + pot;
        }
    }
    LinhaGrande();
    printf("\n BINARIO: %s \n DECIMAL: %d", analisado, decimal);
    LinhaGrande();

}

int decToOct(int entrada, int *valido) {
    int i = 0, j, resto, input, count = 0;
    int backNum[bits];
    input = entrada;
    int num;
    if (input < 1) {
        printf("\n N�mero Decimal Inv�lido. \n");
        return 0;
    }

    while ( resto > 7 ) {
        num = input % 8;
        backNum[i] = num;
        resto = input/8;
        input = resto;
        i++;
    }

    backNum[i] = input;
    int binarioFinal[i];
    count = 0;
    LinhaGrande();
    printf("\ DECIMAL: %d \n OCTAL: ", entrada);
    for (j = i; j > -1; j--) {
        binarioFinal[count] = backNum[j];
        printf("%d", binarioFinal[count]);
        count++;
    }
    LinhaGrande();
    *valido =1;
}

int decToBin(int entrada, int *valido) {
    int i = 0, j, resto, input, count = 0;
    int backBin[bits];
    input = entrada;
    int num;
    if (input < 1) {
        printf("\n N�mero Decimal Inv�lido. \n");
        return 0;
    }

    while ( resto > 1 ) {
        num = input % 2;
        backBin[i] = num;
        resto = input/2;
        input = resto;
        i++;
    }

    backBin[i] = input;
    int binarioFinal[i];
    count = 0;
    LinhaGrande();
    printf("\ DECIMAL: %d \n BIN�RIO: ", entrada);
    for (j = i; j > -1; j--) {
        binarioFinal[count] = backBin[j];
        printf("%d", binarioFinal[count]);
        count++;
    }
    LinhaGrande();
    *valido =1;
}

int potencia (int base, int peso) {
    int saida = 1, i;
    if (peso == 0) {
        return 1;
    }

    for (i = 0; i < peso; i++) {
        saida = saida * base;
    }
    return saida;
}

void LimpaTela() {
	system("CLS");
}

void ExibeMenu() {
    setlocale(LC_ALL, "Portuguese");
    printf("\n a. Binario para Decimal."
       "\n b. Decimal para Binario."
       "\n c. Octal para Decimal."
       "\n d. Decimal para Octal."
       "\n e. Decimal para Hexadecimal."
       "\n f. Hexadeciaml para Decimal."
       "\n g. Para sair.");
    printf("\n\n Voc� quer converter de: ");
}

void Cabecalho(char titulo[50]) {
	LimpaTela();
    LinhaGrande();
	printf("         CONVERSOR DE BASES ");
    LinhaGrande();
}

void LinhaGrande() {
    printf("\n-----------------------------------------------\n");
}

void LinhaMedia() {
    printf("\n------------------------------");
}

void LinhaPequena() {
    printf("\n---------------------");
}


